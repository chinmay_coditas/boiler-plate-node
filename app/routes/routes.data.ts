import { Route } from './routes.types';
import demoRouter from "../demo/demo.routes"
export const routes: Route[] = [
    new Route("/demo", demoRouter),
];