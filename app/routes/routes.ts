import { Application, json, NextFunction, Response, Request } from "express";
import helmet from "helmet";
import { routes } from "./routes.data";

export const registerMiddlewares = (app: Application) => {
    app.use(helmet());
    app.use(json());

    for (let route of routes) {
        app.use(route.path, route.router);
    }

}