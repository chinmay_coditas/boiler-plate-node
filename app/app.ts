import express from "express"
import { registerMiddlewares } from "./routes/routes"

export const startServer = () => {
    const app = express()
    const { PORT } = process.env
    registerMiddlewares(app)
    app.listen( PORT , ( )  => {
        console.log( ` Server Started at ${PORT} ` )
    })
}

